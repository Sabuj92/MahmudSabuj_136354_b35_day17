<?php
class EmployeeInfo{
    public $employee_id="id";
    public $employee_name="";
    public $employee_sallary="";


    public function getEmployeeId()
    {
        return $this->employee_id;
    }

    public function getEmployeeName()
    {
        return $this->employee_name;
    }

    public function getEmployeeSallary()
    {
        return $this->employee_sallary;
    }

    public function setEmployeeId($employee_id){
        $this->employee_id = $employee_id;
    }

    public function setEmployeeName($employee_name)
    {
        $this->employee_name = $employee_name;
    }

    public function setEmployeeSallary($employee_sallary)
    {
        $this->employee_sallary = $employee_sallary;
    }
}
$employee = new EmployeeInfo;
$employee->setEmployeeId("5545");
$employee->setEmployeeName("Rahim Ali");
$employee->setEmployeeSallary("50000");
echo "<b>Employee ID: </b>".$employee->getEmployeeId()."<br>";
echo "<b>Employee Name: </b>".$employee->getEmployeeName()."<br>";
echo "<b>Employee Salary: </b>".$employee->getEmployeeSallary();